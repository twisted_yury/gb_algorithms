/*
Student Likhachev Yury
GeekBrains. Algorithms and data structures course.
Homework 1-2
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define EXIT_CODE 99
#define ARRAY_LENGTH 100
#define ROWS 3
#define COLUMNS 3

#pragma region HOMEWORK1
//Task 1
void BMICalc() {
	float fHeight, fWeight;

	//Description
	printf("Task 1. BMI calculation.\n");

	//Ask height
	printf("\nPlease, enter your height in centimeters: ");
	scanf("%f", &fHeight);
	fHeight /= 100;

	//Ask weight
	printf("\nNow enter your weight in kilograms: ");
	scanf("%f", &fWeight);

	//Print result BMI
	printf("\nYour BMI are: %.2f\n\n", fWeight / (fHeight * fHeight));
}


//Task 2
void GetMaxNumber() {
	int iNumberA, iNumberB, iNumberC, iNumberD;

	//Description
	printf("Task 2. Getting maximum number.\n");

	//Ask numbers
	printf("\nPlease, enter 4 numbers (use space as delimeter): ");
	scanf("%d%d%d%d", &iNumberA, &iNumberB, &iNumberC, &iNumberD);

	//Compare first pair
	if (iNumberA < iNumberB)
		iNumberA = iNumberB;
	//Compare second pair
	if (iNumberC < iNumberD)
		iNumberC = iNumberD;
	//Compare final pair
	if (iNumberA < iNumberC)
		iNumberA = iNumberC;

	//Print maximum number
	printf("\nMaximum number is %d\n\n", iNumberA);
}

//Task 3
void ExchangeValues() {
	int iNumberA, iNumberB;

	//Description
	printf("Task 3. Exchange two integers.\n");

	//Ask numbers
	printf("\nPlease, enter 2 numbers (use space as delimeter): ");
	scanf("%d%d", &iNumberA, &iNumberB);
	printf("\nValues before exchange: number1=%d, number2=%d\n", iNumberA, iNumberB);

	//Exchange
	iNumberA = iNumberA + iNumberB;
	iNumberB = iNumberA - iNumberB;
	iNumberA = iNumberA - iNumberB;

	//Print exchanged integers
	printf("\nValues after exchange: number1=%d, number2=%d\n\n", iNumberA, iNumberB);
}

//Task 4
void PassQuadratic() {
	int iA, iB, iC;
	float fD;

	//Description
	printf("Task 4. Get roots from quadratic.\n");

	//Ask numbers
	printf("\nPlease, enter 3 members of quadratic, a, b and c (use space as delimeter): ");
	scanf("%d%d%d", &iA, &iB, &iC);
	printf("\nQuadratic: %dx^2 + %dx + %d = 0\n", iA, iB, iC);

	//Discriminant calc
	fD = iB * iB - 4 * iA * iC;
	printf("\nDiscriminant are: %.0f\n", fD);

	//Print roots
	if (fD < 0)
		printf("\nQuadratic has no roots.\n\n");
	else if (fD == 0)
		printf("\nRoot are %.2f\n\n", -(iB / 2 * iA));
	else
		printf("\nRoots are: x1=%.2f x2=%.2f\n\n", (-iB + sqrt(fD) / 2 * iA), (-iB - sqrt(fD) / 2 * iA));
}
#pragma endregion HOMEWORK1


#pragma region HOMEWORK2
//Task 1
void DecimalToBinaryCall() {
	int iNumberDecimal;

	//Description
	printf("Task 1. Converting decimal to binary.\n");

	//Ask numbers
	printf("\nPlease, enter a number: ");
	scanf("%d", &iNumberDecimal);

	//Print converted number
	printf("\nDecimal %d is %d binary.\n\n", iNumberDecimal, DecimalToBinaryCalc(iNumberDecimal));
}

//Recursive function to convert decimal input into binary output
int DecimalToBinaryCalc(int iNumberDecimal) {
	if (iNumberDecimal == 1)
		return 1;
	else
		return DecimalToBinaryCalc(iNumberDecimal / 2) * 10 + iNumberDecimal % 2;
}

//Task 2
void APowBCall() {
	int iNumber, iPower;

	//Description
	printf("Task 2. Calculate a number A in power B (with and without recursion).\n");

	//Ask numbers
	printf("\nPlease, enter a number and power (use space as delimeter): ");
	scanf("%d%d", &iNumber, &iPower);

	//Print results
	printf("\nRegular function. Number %d in power %d are: %d.\n", iNumber, iPower, APowB(iNumber, iPower));
	printf("\nRecursive function. Number %d in power %d are: %d.\n\n", iNumber, iPower, APowBRecursive(iNumber, iPower));
}

//Calc power by loop
int APowB(int iNumber, int iPower) {
	int iResult = iNumber;
	while (iPower > 1)
	{
		iResult *= iNumber;
		iPower--;
	}
	return iResult;
}

//Calc power by recursion
int APowBRecursive(int iNumber, int iPower) {
	if (iPower > 1)
		return APowBRecursive(iNumber, iPower - 1) * iNumber;
	else
		return iNumber;
}

//Task 3
void SolutionCalculatorCall() {
	int iStartValue = 3;
	int iResultValue = 20;

	//Description
	printf("Task 3. Calculate solutions count to convert number %d to %d with +1 and *2 operations.\n", iStartValue, iResultValue);

	//Print results
	printf("\nCalculation with array. Where are %d solutions to convert number %d to %d with +1 and *2 operations.\n", SolutionCalculatorArray(iStartValue, iResultValue), iStartValue, iResultValue);
	//printf("\nRecursive function. Number %d in power %d are: %d.\n\n", iNumber, iPower, APowBRecursive(iNumber, iPower));
}

int SolutionCalculatorArray(int iStartValue, int iReslutValue) {
	int iaSolutionsMatrix[20][20][20];
	int iSolutionsCount = 0;

	//Populate array with zeroes and ones. Zero means +1, one means *2
	for (int i = 0; i < 20; i++)
	{
		for (int j = 0; j < 20; j++) {
			for (int k = 0; k < 20; k++) {
				if (k <= j || k == i)
					iaSolutionsMatrix[i][j][k] = 1;
				else
					iaSolutionsMatrix[i][j][k] = 0;
			}
		}
	}



	for (int i = 0; i < 20; i++)
	{
		for (int j = 0; j < 20; j++) {
			for (int k = 0; k < 20; k++) {
				printf("%d", iaSolutionsMatrix[i][j][k]);
			}
			printf("\n");
		}
	}


	return iSolutionsCount;
}

#pragma endregion HOMEWORK2


#pragma region HOMEWORK3
//Task 1

void PrintArray(int *iArray, int iArrayLength)
{
	for (int i = 0; i < iArrayLength; i++)
		printf("%d ", iArray[i]);
	printf("\n");
}

void BubbleSort() {
	int iARandoms[ARRAY_LENGTH];
	int iASortedBubble[ARRAY_LENGTH];
	int iASortedBubbleImproved[ARRAY_LENGTH];
	int iTemp;
	int iActionCount = 0;

	//Description
	printf("Task 1. Array bubble sort.\n");

	//Array filling by random
	for (int i = 0; i < ARRAY_LENGTH; i++)
		iARandoms[i] = rand() % 100;

	//Copy values to sorted
	for (int i = 0; i < ARRAY_LENGTH; i++) {
		iASortedBubble[i] = iARandoms[i];
		iASortedBubbleImproved[i] = iARandoms[i];
	}

	//Display unsorted array
	printf("\nUnsorted array looks like:\n");
	PrintArray(iARandoms, ARRAY_LENGTH);


	//Sorting array by default method
	for (int i = 0; i < ARRAY_LENGTH; i++) {
		for (int j = 0; j < ARRAY_LENGTH - 1; j++) {
			if (iASortedBubble[j] > iASortedBubble[j + 1]) {
				iTemp = iASortedBubble[j];
				iASortedBubble[j] = iASortedBubble[j + 1];
				iASortedBubble[j + 1] = iTemp;
				iActionCount += 3;
			}
			iActionCount += 3;
		}
		iActionCount++;
	}

	//Display array sorted by default method and operations count
	printf("\nSorted by default bubble sort array looks like:\n");
	PrintArray(iASortedBubble, ARRAY_LENGTH);
	printf("\nOperations counts are: %d. Array length is: %d.\n", iActionCount, ARRAY_LENGTH);


	//Reset count
	iActionCount = 0;

	//Sorting array by improved method. Added flag for early exit
	int iFlag;
	for (int i = 0; i < ARRAY_LENGTH; i++) {
		iFlag = 0;
		for (int j = 0; j < ARRAY_LENGTH - 1; j++) {
			if (iASortedBubbleImproved[j] > iASortedBubbleImproved[j + 1]) {
				iTemp = iASortedBubbleImproved[j];
				iASortedBubbleImproved[j] = iASortedBubbleImproved[j + 1];
				iASortedBubbleImproved[j + 1] = iTemp;
				iFlag++;
				iActionCount += 4;
			}
			iActionCount += 3;
		}
		if (iFlag == 0)
			break;
		iActionCount+=3;
	}

	//Display array sorted by default method and operations count
	printf("\nSorted by improved bubble sort array looks like:\n");
	PrintArray(iASortedBubbleImproved, ARRAY_LENGTH);
	printf("\nOperations counts are: %d. Array length is: %d.\n\n", iActionCount, ARRAY_LENGTH);

}


//Task 2

void ShakerSort() {
	int iA[ARRAY_LENGTH];
	int iTempValue, iTempCounter, i;
	
	//Description
	printf("Task 2. Array shaker sort.\n");

	//Array filling by random
	for (int i = 0; i < ARRAY_LENGTH; i++)
		iA[i] = rand() % 100;

	//Display unsorted array
	printf("\nUnsorted array looks like:\n");
	//Function described in Task 1
	PrintArray(iA, ARRAY_LENGTH);

	for (i = 0; i < ARRAY_LENGTH / 2; i++) {
		//Go from left to right. If value lower - remember it and it place in array
		iTempValue = iA[i];
		for (int j = i; j < ARRAY_LENGTH - 1; j++) {
			if (iA[j + 1] < iTempValue) {
				iTempValue = iA[j + 1];
				iTempCounter = j + 1;
			}
		}
		iA[iTempCounter] = iA[i];
		iA[i] = iTempValue;

		//Go from right to left. If value higher - remember it and it place in array
		iTempValue = iA[ARRAY_LENGTH - i - 1];
		for (int j = ARRAY_LENGTH - i - 1; j > i + 1; j--) {
			if (iA[j - 1] > iTempValue) {
				iTempValue = iA[j - 1];
				iTempCounter = j - 1;
			}
		}
		iA[iTempCounter] = iA[ARRAY_LENGTH - i - 1];
		iA[ARRAY_LENGTH - i - 1] = iTempValue;
	}

	//Display array sorted by shaker method and operations count
	printf("\nSorted by shaker method array looks like:\n");
	PrintArray(iA, ARRAY_LENGTH);
	printf("\nArray length is: %d.\n\n", ARRAY_LENGTH);

}


//Task 3

void BinarySearchCall() {
	int iAOrdered[ARRAY_LENGTH];
	int iValue;

	//Description
	printf("Task 3. Binary search function.\n");

	//Array filling by random
	for (int i = 0; i < ARRAY_LENGTH; i++)
		iAOrdered[i] = i + 100;

	//Display unsorted array
	printf("\nUnsorted array looks like:\n");
	//Function described in Task 1
	PrintArray(iAOrdered, ARRAY_LENGTH);

	//Ask value
	printf("\nPlease, enter value to search: ");
	scanf("%d", &iValue);

	//Print search result
	printf("\nRequired value has index: %d\n\n", BinarySearch(iAOrdered, iValue, ARRAY_LENGTH));
}

int BinarySearch(int *iArray, int iReqValue, int iArrayLength) {
	int result = -1;
	int iCurrentIndex = iArrayLength / 2;
	int iStep = iCurrentIndex / 2;

	while (result == -1 && iCurrentIndex >= 0 && iCurrentIndex < iArrayLength) {
		if (iReqValue > iArray[iCurrentIndex]) {
			iCurrentIndex += iStep;
		}
		else if (iReqValue < iArray[iCurrentIndex]) {
			iCurrentIndex -= iStep;
		}
		else {
			result = iCurrentIndex;
		}

		//Halfing step
		if ((iStep / 2) > 0)
			iStep /= 2;
		else
			iStep = 1;
	}
	return result;
}

#pragma endregion HOMEWORK3


#pragma region HOMEWORK4
//Task 1

void PrintBoard(int iArray[ROWS][COLUMNS], int n, int m)
{
	int i, j;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
			printf("%4d", iArray[i][j]);
		printf("\n");
	}
}

void GetRoute() 
{
	int iMap[ROWS][COLUMNS] = {
	{1, 1, 1},
	{0, 1, 0},
	{0, 1, 0}
	};
	int iA[ROWS][COLUMNS];
	int i, j;

	//Description
	printf("Task 1. Get routes count.\n");

	//Check and fill starting point
	if (iMap[0][0] == 0) {
		printf("Where are no routes at all!");
		return;
	}
	else
		iA[0][0] = 1;

	//Check and fill first row with the exeption of starting point
	for (j = 1; j < COLUMNS; j++) {
		if (iMap[0][j] == 1 && iA[0][j - 1] != 0)
			iA[0][j] = 1;
		else
			iA[0][j] = 0;
	}

	for (i = 1; i < ROWS; i++)
	{
		//Check and fill first column with the exeption of starting point
		if (iMap[i][0] == 1 && iA[i - 1][0] != 0)
			iA[i][0] = 1;
		else
			iA[i][0] = 0;
		
		for (j = 1; j < COLUMNS; j++)
		{
			if (iMap[i][j] == 0)
				iA[i][j] = 0;
			else
				iA[i][j] = iA[i][j - 1] + iA[i - 1][j];
		}
	}
	PrintBoard(iA, ROWS, COLUMNS);
}

//Task 2

void FindSubsequence()
{
	char sOne[] = "GEEKBRAINS";
	char sTwo[] = "GEEKMINDS";
	
	int iRows = sizeof(sOne) / sizeof(sOne[0]) - 1;
	int iColumns = sizeof(sTwo) / sizeof(sTwo[0]) -1;

	char sShared[20];
	int iSharedCount = 0;

	//Description
	printf("Task 2. Find shared subsequence in 2 strings.\n");

	for (int i = iRows - 1; i >= 0; i--) {
		for (int j = iColumns - 1; j >= 0; j--) {
			if (sOne[i] == sTwo[j]) {
				iSharedCount++;
				sShared[iSharedCount] = sOne[i];
				break;
			}
		}
	}

	//Print result
	printf("\nMaximum shared subsequence are: ");
	for (int i = iSharedCount; i > 0; i--)
		putchar(sShared[i]);
	printf("\nWith length %d.\n\n", iSharedCount);


}

#pragma endregion HOMEWORK4

int main() {

	int iCurrentTask = 0;

	//Tasks menu
	while (iCurrentTask != EXIT_CODE) {
		printf("Enter number.\n");
		printf("1: Homework 1. Task 1. BMI calculation.\n");
		printf("2: Homework 1. Task 2. Getting maximum number.\n");
		printf("3: Homework 1. Task 3. Exchange two integers.\n");
		printf("4: Homework 1. Task 4. Get roots from quadratic.\n");
		printf("5: Homework 2. Task 1. Converting decimal to binary.\n");
		printf("6: Homework 2. Task 2. Calculate a number A in power B (with and without recursion).\n");
		printf("7: Homework 2. Task 3. INCOMPLETE. Calculate solutions count to convert number 3 to 20 with +1 and *2 operations.\n");
		printf("8: Homework 3. Task 1. Array bubble sort.\n");
		printf("9: Homework 3. Task 2. Array shaker sort.\n");
		printf("10: Homework 3. Task 3. Binary search function.\n");
		printf("11: Homework 4. Task 1. Get routes count.\n");
		printf("12: Homework 4. Task 2. Find shared subsequence in 2 strings.\n");
		printf("%d: Exit.\n", EXIT_CODE);

		//Check input
		if (scanf("%d", &iCurrentTask) == 1)
			switch (iCurrentTask)
			{
			case 1:
				BMICalc();
				break;
			case 2:
				GetMaxNumber();
				break;
			case 3:
				ExchangeValues();
				break;
			case 4:
				PassQuadratic();
				break;
			case 5:
				DecimalToBinaryCall();
				break;
			case 6:
				APowBCall();
				break;
			case 7:
				SolutionCalculatorCall();
				break;
			case 8:
				BubbleSort();
				break;
			case 9:
				ShakerSort();
				break;
			case 10:
				BinarySearchCall();
				break;
			case 11:
				GetRoute();
				break;
			case 12:
				FindSubsequence();
				break;
			default:
				break;
			}
		//Clear input buffer
		else 
			scanf("%*[^\n]");

	}
	return 0;
}